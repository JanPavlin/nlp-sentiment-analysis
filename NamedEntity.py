class NamedEntity:
    def __init__(self, ID, lemma):
        self.ID = ID
        self.lemma = lemma
        self.words = set([lemma])
        self.appears = 1
        self.sentiment = ""
        self.connectedb = set([])  # Bottom connections
        self.connectedt = set([])  # Top connections

    def toString(self):
        print("Entity " + str(self.ID))
        print(self.words)
        print("Appearances: " + str(self.appears))
        print("Sentiment: " + self.sentiment)