import pandas as pd
import numpy as np
import pickle as pkl
import time
from sklearn import svm
from sklearn.metrics import classification_report
from Parser import removeStopWords, getStopWords
from sklearn.feature_extraction.text import TfidfVectorizer
from NamedEntity import *


def prepareEntities(use_neutral=True, e_words=True, e_connectdb=True, e_connectedt=True, data_2=False):
    '''
    The pre-processed data from our dataset is saved in file (it is something like "entities.pll"). In this function the
    processed data is read from file and prepared for machine learning.
    '''
    sentences = pd.DataFrame({'content': [], 'sentiment': []})

    if not data_2:
        # Usual processing if second dataset is not used:
        Entities = pkl.load(open("entities_10.5.pkl", "rb"))

        for E in Entities:
            E = removeStopWords(E, getStopWords())
            string = ""
            stringWords = " ".join(E.words)
            stringConnectedb = " ".join(E.connectedb)
            stringConnectedt = " ".join(E.connectedt)
            if e_words:
                string += stringWords
            if e_connectdb:
                string += " " + stringConnectedb
            if e_connectedt:
                string += " " + stringConnectedt

            df2 = pd.DataFrame({'content': [string], 'sentiment': [E.sentiment]})

            if not use_neutral:
                if E.sentiment not in ['Nevtralno', '']:
                    sentences = sentences.append(df2)
            else:
                if E.sentiment != '':
                    sentences = sentences.append(df2)

        sentences["polarity"] = np.where(sentences["sentiment"] == 'Nevtralno', 'Nepolarno', 'Polarno')
    else:

        fname = "SentiCoref_1.0/anotated/SentiNews_document-level.txt"

        tsvfile = pd.read_csv(fname, sep='\t')
        df = tsvfile[tsvfile.sentiment != "neutral"].reset_index(drop=True)
        df = df.drop(["content"], axis=1)
        df = df.rename(columns={'keywords': 'content'})

        df["content"] = df["content"].astype(str).str.replace(",", "")
        df["sentiment"] = np.where(df["sentiment"] == 'positive', 'Pozitivno', 'Negativno')
        df = df[['content', 'sentiment']]
        sentences = df

    return sentences


def classification_SVM(train, test, save_model=True, use_neutral=True, polar=False):
    '''
    Uses SVM classification of input data. Thes function is able to classify non-neutral, neutral or even polar data.
    At first, the Tf-idf vectorizer is used to transform data. With transformed data Classifier is learned on train data
    and evaluated on test data. Both times for learning and predicting are reported. From sklearn metrics library the
    classification_report() function is used to report the result of our classifier.
    '''
    vectorizer = TfidfVectorizer(min_df=5,
                                 max_df=0.8,
                                 sublinear_tf=True,
                                 use_idf=True)

    train_vectors = vectorizer.fit_transform(train['content'])
    test_vectors = vectorizer.transform(test['content'])

    classifier_linear = svm.SVC(kernel='rbf')
    t0 = time.time()

    if polar:
        classifier_linear.fit(train_vectors, train['polarity'])
    else:
        classifier_linear.fit(train_vectors, train['sentiment'])
    t1 = time.time()
    prediction_linear = classifier_linear.predict(test_vectors)
    t2 = time.time()
    time_linear_train = t1-t0
    time_linear_predict = t2-t1

    print("Training time: %fs; Prediction time: %fs" % (time_linear_train, time_linear_predict))

    if polar:
        report = classification_report(test['polarity'], prediction_linear, output_dict=True)
        print('Polarno: ', report['Polarno'])
        print('Nepolarno: ', report['Nepolarno'])

    else:
        report = classification_report(test['sentiment'], prediction_linear, output_dict=True)
        print('positive: ', report['Pozitivno'])
        print('negative: ', report['Negativno'])
        if use_neutral:
            print('neutral: ', report['Nevtralno'])

        if save_model:
            pkl.dump(vectorizer, open('models/SVM_vectorizer.sav', 'wb'))
            pkl.dump(classifier_linear, open('models/SVM_classifier.sav', 'wb'))

'''
Without neutral:
Training time: 0.654814s; Prediction time: 0.080305s
    positive:  {'precision': 0.6626984126984127, 'recall': 0.7016806722689075, 'f1-score': 0.6816326530612244, 'support': 238}
    negative:  {'precision': 0.7137096774193549, 'recall': 0.6755725190839694, 'f1-score': 0.6941176470588234, 'support': 262}
________________________________________________________________________________________________________________________
With neutral: 
Training time: 13.598563s; Prediction time: 0.280187s
    positive:  {'precision': 0.5, 'recall': 0.05263157894736842, 'f1-score': 0.09523809523809525, 'support': 19}
    negative:  {'precision': 0.4, 'recall': 0.06666666666666667, 'f1-score': 0.1142857142857143, 'support': 30}
    neutral:  {'precision': 0.9066937119675457, 'recall': 0.991130820399113, 'f1-score': 0.9470338983050848, 'support': 451}
'''
