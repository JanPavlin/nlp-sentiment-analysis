import pandas as pd
import re
import string
import csv
# import nltk
import lemmagen.lemmatizer
from lemmagen.lemmatizer import Lemmatizer
import stanza
from caffe2.python import workspace


# Class objects
class NamedEntity:
    def __init__(self, ID, lemma):
        self.ID = ID
        self.lemma = lemma
        self.words = set([lemma])
        self.appears = 1
        self.sentiment = ""
        self.connectedb = set([])  # Bottom connections
        self.connectedt = set([])  # Top connections

    def toString(self):
        print("Entity " + str(self.ID))
        print(self.words)
        print("Appearances: " + str(self.appears))
        print("Sentiment: " + self.sentiment)


bannedPosTags = ["NOUN", "PRON", "PROPN", "PUNCT", "NUM"]
negationWords = ["ne", "ni"]


# Opens a .tsv file with filename and extracts tabular lines
def parseentry(fname):
    tsvfile = pd.read_csv(fname, sep='\t', skiprows=7, header=None, quoting=csv.QUOTE_NONE,
                          converters={i: str for i in range(0, 10)})
    # print(tsvfile.head(10))
    return tsvfile


# Extracts word column from the table as a string
def getText(tsvf):
    coll = tsvf[2][:]
    string = ""
    for c in coll:
        string += " " + c
    return string


# Gets stop word list from the text file
def getStopWords():
    f = open('slovenian-stopwords.txt', 'r', encoding="utf-8")
    l = [s.strip(' ') for s in f.read().splitlines()]
    f.close()
    return l


def removeStopWords(Entity, stopwords):
    # print("Removing stopwords...")
    listw = Entity.connectedb.copy()
    for w in listw:
        # print(w)
        if (w in stopwords or w in string.punctuation):
            Entity.connectedb.discard(w)
    listw = Entity.connectedt.copy()
    for w in listw:
        # print(w)
        if (w in stopwords or w in string.punctuation):
            Entity.connectedt.discard(w)
    return Entity


# Replace/merge words that negate meaning
def removeNegations(tsvf):
    print("Removing negations...")
    replacements = 0
    rows = len(tsvf)
    i = 0
    while i < rows - 1:
        # print(tsvf[2][i])
        if (tsvf[2][i].lower() == "ne" or tsvf[2][i].lower() == "ni"):
            if (tsvf[2][i + 1] not in [".", ",", "?", "!", "(", ")"]):
                # print("=Found negation to remove.")
                tsvf[2][i + 1] = tsvf[2][i] + "" + tsvf[2][i + 1]
                tsvf = tsvf.drop(i)
                replacements = replacements + 1
        i = i + 1
    return tsvf, replacements


# Extract word column as tokenized strings
def getSentences(tsvf):
    # List stavkov
    sentences = []
    labels = []
    # Posamezen stavek - list žetonov
    sentence = []
    entities = []
    # Entity pointer
    index = 0
    word = ""
    for index, row in tsvf.iterrows():
        word = row[2]
        indexs = row[6]
        # print(indexs)
        if (indexs == "_"):
            index = [0]
        else:
            m = re.findall(r"\d+", indexs)
            for i in range(0, len(m)):
                m[i] = int(m[i])
            index = m
            # print(word)
            # print(index)
        sentence.append(word)
        entities.append(index)

        # Finish sentence if the word is punctuation
        if (word == "." or word == "?" or word == "!"):
            # print(sentence)
            # print(entities)
            sentences.append(sentence)
            labels.append(entities)
            sentence = []
            entities = []

    if (sentence != []):
        sentences.append(sentence)
        labels.append(entities)
    return sentences, labels


# Semantic stuff with sentences (list of lists of tokens)
def stanzaPipe(sents, entities):
    print("Semantic analysis of sentences")
    # print(sents)
    pipe = stanza.Pipeline('sl', processors='tokenize,mwt,pos,lemma,depparse', tokenize_pretokenized=True)
    parsed = pipe(sents)
    print("Analysis complete.")
    for i, sentence in enumerate(parsed.sentences):
        for j, token in enumerate(sentence.tokens):
            # print(token.words[0].head)
            token.entityID = entities[i][j]

    # for i, sentence in enumerate(parsed.sentences):
    # print(f'====== Sentence {i+1} tokens ============')
    # print(*[f'id: {token.id}\ttext: {token.words[0].lemma} \tPOS: {token.words[0].upos} \tPOS: {token.words[0].xpos}' for token in sentence.tokens], sep='\n')
    return parsed


# Isolate named entities and their detected sentiment
def namedEntities(parsed):
    print("Looking for entities ==============")
    Entities = []
    # Each entity is going to be an object containing ID, list of words and lemmatized form, as well as sentiment
    # Loop through words in sentences
    for i, sentence in enumerate(parsed.sentences):
        for j, token in enumerate(sentence.tokens):

            if (token.entityID != [0]):
                # print(token.entityID)
                # we found a named entity
                # Is an entity with this ID already in Entities?

                for idn in token.entityID:
                    ID = 0
                    # print("Current id: "+str(idn))
                    for e in Entities:
                        if (e.ID == idn):
                            # print("Found matching entity")
                            # Just modify existing entity
                            e.words.add(token.words[0].lemma)
                            e.appears += 1
                            ID = e.ID
                    if (ID == 0):
                        # No match found: create new one
                        # print("Creating new entity")
                        # print(token.text)
                        ent = NamedEntity(idn, token.words[0].lemma)
                        Entities.append(ent)

    return Entities


# Get sentiment already?
def setSentiment(Entities, tsvf):
    # print("Extracting sentiment ===============")
    for index, row in tsvf.iterrows():
        if (row[4] != "_"):
            # Extract ID

            ID = row[6][:-1]
            m = re.findall(r"\d+", ID)
            for i in range(0, len(m)):
                m[i] = int(m[i])
            ID = m
            # print(ID)
            # Find entity with ID, set
            for e in Entities:
                # print(e.ID)
                # print(ID)
                for idm in ID:
                    if e.ID == idm:
                        # Here is where we set the class
                        if "Pozitivno" in row[4]:
                            e.sentiment = "Pozitivno"
                        elif "Negativno" in row[4]:
                            e.sentiment = "Negativno"
                        elif "Nevtralno" in row[4]:
                            e.sentiment = "Nevtralno"
                        # print(e.sentiment)


# Get connected words
# Words whose head is entity

# Mogoče filtriraj besede še po vrsti? Da ne vključujemo drugih lastnih imen
# TO BE IMPROVED
def getConnectedUnder(Entity, parsed, entities):
    # print(Entity.words)
    for ind, ents in enumerate(entities):
        # print(Entity.ID)
        indexPosList = [i for i in range(len(ents)) if Entity.ID in ents[i]]

        if indexPosList != []:
            # print(ents)
            # print(indexPosList)
            sentence = parsed.sentences[ind]
            # print(sentence)
            for i in indexPosList:
                sentid = sentence.tokens[i].id
                senthd = sentence.tokens[i].words[0].head
                # print("Sentence ID of entity: "+str(sentid))
                # print("Sentence Head of entity: "+str(senthd))
                # Find all words which have sentid as head
                for token in sentence.tokens:
                    # if token ID is different from ones in indexPosList+1
                    # if (int(token.id)-1 not in indexPosList):
                    # print(token.text +" " + token.id+ " "+ str(token.words[0].head)+" "+str(token.words[0].upos))
                    if (headLink(sentence, token, sentid) and token.words[0].upos not in bannedPosTags):
                        Entity.connectedb.add(token.words[0].lemma)
                    if (int(token.words[0].head) == int(sentid) and token.words[0].upos not in bannedPosTags):
                        Entity.connectedb.add(token.words[0].lemma)
                        # print("== match B ==")
                    if (int(token.id) == int(senthd) and token.words[0].upos not in bannedPosTags):
                        Entity.connectedt.add(token.words[0].lemma)
                        # print("== match T ==")
    return Entity


def headLink(sentence, token, headid):
    # Returns true if there is a chain of head connections from a token in sentence to certain id
    # print(str(token.words[0].head)+" "+str(headid))
    if (int(token.words[0].head) == int(headid)):
        return True
    # Find the head word of that token
    # print("Othertokens")
    for token2 in sentence.tokens:
        if (token2.id == int(token.words[0].head)):
            return (headLink(sentence, token2, headid))
    return False


def getConnectedAll(Ents, parsed, entities):
    for i, E in enumerate(Ents):
        Ents[i] = getConnectedUnder(E, parsed, entities)
    return Ents


if __name__ == '__main__':

    table = parseentry("../SEMINARSKA/SentiCoref_1.0/122.tsv")
    # print(table.columns)
    # print(getText(table))
    table, replacements = removeNegations(table)
    print("Replaced negations: " + str(replacements))

    sents, entities = getSentences(table)

    print("Detected " + str(len(sents)) + " sentences.")
    # print(sents[-1])

    parsed = stanzaPipe(sents, entities)
    Entities = namedEntities(parsed)
    setSentiment(Entities, table)

    Entities = getConnectedAll(Entities, parsed, entities)

    Stops = getStopWords()
    # print(Stops)


    for E in Entities:
        E = removeStopWords(E, Stops)
        print("========")
        E.toString()
        print(E.connectedb)
        print(E.connectedt)
