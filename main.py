from classification_SVM import classification_SVM, prepareEntities
from classification_multiple import classification_multiple
from NamedEntity import *
import numpy as np
from sklearn.model_selection import StratifiedKFold

from utils import *
from second_dataset import classification_second_dataset

SAVE_MODEL = True
USE_NEUTRAL = False
USE_POLAR = False
INCLUDE_SECOND_DATASET = False

N_SPLIT = 3
classifier_list = ['nb', 'maxent', 'svm']
multimodel_scores = {}
for i in range(len(classifier_list)):
    multimodel_scores[classifier_list[i]] = [0 for _ in range(7)]


def learning():
    '''
    Main part of machine learning starts in this function. If INCLUDE_SECOND_DATASET parameter is set to True, second
    dataset will also be included in training set.

    DataFrame is prepared with prepareEntities() function and is split into n-splits with same distribution of
    class values.

    On each splitted part, two types of learning are used, first one is classification with only SVM classifier and the
    second one is is classification with multiple classifiers (this are naive-bayes, maximum entropy and SVM classifier.
    '''

    print("Starting with learning")
    if INCLUDE_SECOND_DATASET:
        classification_second_dataset(USE_NEUTRAL, USE_POLAR, N_SPLIT)

    skf = StratifiedKFold(n_splits=N_SPLIT, random_state=None, shuffle=False)
    res_multimodel = []

    df = prepareEntities(use_neutral=USE_NEUTRAL)
    for train_index, test_index in skf.split(df['content'], df['sentiment']):
        train = df.iloc[train_index]
        test = df.iloc[test_index]

        classification_SVM(train, test, use_neutral=USE_NEUTRAL, polar=USE_POLAR)
        res_multimodel.append(classification_multiple(train, test, use_polar=USE_POLAR))

    for row in res_multimodel:
        for key, arr in row.items():
            multimodel_scores[key] = [x + y for x, y in
                                      zip(multimodel_scores[key], (np.array(arr) / N_SPLIT))]
    #multiscore_report(multimodel_scores)


if __name__ == '__main__':
    learning()


'''
Starting with learning
Training time: 0.305021s; Prediction time: 0.114988s
positive:  {'precision': 0.6429809358752167, 'recall': 0.6429809358752167, 'f1-score': 0.6429809358752167, 'support': 577}
negative:  {'precision': 0.6578073089700996, 'recall': 0.6578073089700996, 'f1-score': 0.6578073089700996, 'support': 602}
Training time: 0.305003s; Prediction time: 0.118996s
positive:  {'precision': 0.6313432835820896, 'recall': 0.734375, 'f1-score': 0.6789727126805778, 'support': 576}
negative:  {'precision': 0.6994106090373281, 'recall': 0.5903814262023217, 'f1-score': 0.6402877697841728, 'support': 603}
Training time: 0.318991s; Prediction time: 0.123999s
positive:  {'precision': 0.6684684684684684, 'recall': 0.6440972222222222, 'f1-score': 0.6560565870910698, 'support': 576}
negative:  {'precision': 0.6709470304975923, 'recall': 0.6943521594684385, 'f1-score': 0.6824489795918368, 'support': 602}
'''