import pandas as pd
import re
import string
# import nltk
import lemmagen.lemmatizer
from lemmagen.lemmatizer import Lemmatizer
import stanza
from caffe2.python import workspace
import pickle as pkl
import numpy as np
import os
import csv
from Parser import *


def preprocess(filename):
    table = parseentry(filename)
    # print(table.columns)
    # print(getText(table))

    sents, entities = getSentences(table)

    # print("Detected " + str(len(sents)) + " sentences.")
    # print(sents[-1])

    parsed = stanzaPipe(sents, entities)
    Entities = namedEntities(parsed)
    setSentiment(Entities, table)

    Entities = getConnectedAll(Entities, parsed, entities)

    return Entities

def savedataPickle():
    Entities = []
    senti_path = r'./SentiCoref_1.0'
    files = list()
    for file in os.listdir(senti_path):
        if file.endswith(".tsv"):
            files.append(file)
    n = 0

    for el in files:
        n += 1

        print(el)
        print(n)
        try:
            Entities = Entities + preprocess(senti_path + '/' + el)
        except:
            errors.append(el)
    pkl.dump(Entities, open(
        "./entities_24.5.pkl", "wb"))


if __name__ == '__main__':
    errors = []
    SAVED = False
    if not SAVED:
        savedataPickle()

    Entities = pkl.load(open(
        "./entities_24.5.pkl", "rb"))
    pos = []
    neu = []
    neg = []

    polar = []
    non_polar = []
    sentiment = []

    i = 0
    for E in Entities:
        i += 1

        sentiment.append(E.sentiment)
        if E.sentiment == 'Nevtralno':
            neu.append((E.connectedt, E.sentiment))
            non_polar.append((E.connectedt, 'Nepolarno'))
        elif E.sentiment == 'Pozitivno':
            pos.append((E.connectedt, E.sentiment))
            polar.append((E.connectedt, 'Polarno'))
        elif E.sentiment == 'Negativno':
            neg.append((E.connectedt, E.sentiment))
            polar.append((E.connectedt, 'Polarno'))
        else:
            print("PROBLEM SENTIMENTA:", E.sentiment)
            print(errors)

    print("Polarno: ", len(polar))
    print("NePolarno: ", len(non_polar))

    print("Pozitivno: ", len(pos))
    print("Negativno: ", len(neg))
    print("Nevtralno: ", len(neu))

    print("Unikatni sentimenti:", np.unique(sentiment))

    print("Errors: ", errors)

    '''
    Polarno:  1674
    NePolarno:  4185
    Pozitivno:  1674
    Negativno:  0
    nevtralno:  4185
    '''