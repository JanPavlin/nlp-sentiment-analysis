def multiscore_report(score):
    for key, arr in score.items():
        print('---------------------------------------')
        print('RESULT ' + '(' + key + ')')
        print('---------------------------------------')
        print('accuracy:', arr[0])
        print('precision', (arr[1] + arr[2]) / 2)
        print('recall', (arr[3] + arr[4]) / 2)
        print('f-measure', (arr[5] + arr[6]) / 2)