import collections
import nltk.classify.util, nltk.metrics
from nltk.classify import NaiveBayesClassifier, MaxentClassifier, SklearnClassifier
from sklearn.svm import LinearSVC, SVC
from NamedEntity import *


def word_split(data):
    '''
    Returns a list of words that are included in DataFrame.
    '''
    data_new = list()
    for word in data:
        word_filter = [i.lower() for i in word.split()]
        data_new.append(word_filter)
    return data_new


def word_feats(words):
    return dict([(word, True) for word in words])


def evaluate_classifier(train_data, test_data, printit=False):
    '''
    Machine learning with multiple classificators.
    In this function the Naive-Bayes, maximum entropy and SVM classificators are used to learn on data.
    Function returns a list of lists where each element of the list presents one classificator and values within are
    accuracy, precision recall and F-score for positive and negative samples.
    '''
    classifier_list = ['nb', 'maxent', 'svm']
    n = 0
    classifier, classifierName = None, None
    results = dict()

    for cl in classifier_list:

        if cl == 'maxent':
            classifierName = 'Maximum Entropy'
            classifier = MaxentClassifier.train(train_data, trace=0, max_iter=1)
        elif cl == 'svm':
            classifierName = 'Support Vector Machine'
            classifier = SklearnClassifier(LinearSVC())
            classifier.train(train_data)
        elif cl == 'nb':
            classifierName = 'Naive Bayes'
            classifier = NaiveBayesClassifier.train(train_data)

        else:
            exit(-2)
        refsets = collections.defaultdict(set)
        testsets = collections.defaultdict(set)

        for i, (feats, label) in enumerate(test_data):
            refsets[label].add(i)
            observed = classifier.classify(feats)
            testsets[observed].add(i)

        accuracy = nltk.classify.util.accuracy(classifier, test_data)
        pos_precision = nltk.precision(refsets['pos'], testsets['pos'])
        pos_recall = nltk.recall(refsets['pos'], testsets['pos'])
        pos_fmeasure = nltk.f_measure(refsets['pos'], testsets['pos'])
        neg_precision = nltk.precision(refsets['neg'], testsets['neg'])
        neg_recall = nltk.recall(refsets['neg'], testsets['neg'])
        neg_fmeasure = nltk.f_measure(refsets['neg'], testsets['neg'])

        if printit:
            print()
            print('---------------------------------------')
            print('RESULT ' + '(' + classifierName + ')')
            print('---------------------------------------')
            print('accuracy:', accuracy)
            print('precision', (pos_precision + neg_precision) / 2)
            print('recall', (pos_recall + neg_recall) / 2)
            print('f-measure', (pos_fmeasure + neg_fmeasure) / 2)
        results[classifier_list[n]] = [accuracy, pos_precision, neg_precision, pos_recall, neg_recall,
                                       pos_fmeasure, neg_fmeasure]
        n += 1
    return results


def classification_multiple(train_data, test_data, use_polar=False, use_neutral=False):
    '''
    This function uses data based on 'use_polar' and 'use_neutral' parameters and prepares it for learning.
    '''
    if use_polar:
        trainPos = train_data.loc[train_data['polarity'] == 'Polarno']['content'].to_list()
        trainNeg = train_data.loc[train_data['polarity'] == 'Nepolarno']['content'].to_list()
        testPos = test_data.loc[test_data['polarity'] == 'Polarno']['content'].to_list()
        testNeg = test_data.loc[test_data['polarity'] == 'Nepolarno']['content'].to_list()
    else:
        trainPos = train_data.loc[train_data['sentiment'] == 'Pozitivno']['content'].to_list()
        trainNeg = train_data.loc[train_data['sentiment'] == 'Negativno']['content'].to_list()
        testPos = test_data.loc[test_data['sentiment'] == 'Pozitivno']['content'].to_list()
        testNeg = test_data.loc[test_data['sentiment'] == 'Negativno']['content'].to_list()

    if use_neutral:
        #Working with neutral data has not been implemented for this classifier
        train_neudata = train_data.loc[train_data['sentiment'] == 'Nevtralno']['content'].to_list()
        test_neudata = test_data.loc[test_data['sentiment'] == 'Nevtralno']['content'].to_list()
    feat = word_feats

    train = [(feat(f), 'neg') for f in word_split(trainNeg)] + \
            [(feat(f), 'pos') for f in word_split(trainPos)]

    test = [(feat(f), 'neg') for f in word_split(testNeg)] +\
           [(feat(f), 'pos') for f in word_split(testPos)]

    return evaluate_classifier(train, test)
