from classification_SVM import classification_SVM, prepareEntities
from classification_multiple import classification_multiple
from NamedEntity import *
import numpy as np
from utils import *
from sklearn.model_selection import StratifiedKFold


def classification_second_dataset(use_neutral, use_polar, N_SPLIT):
    '''
    In this function features from second dataset are used and added to training samples.
    It helps like nothing ^^
    '''
    classifier_list = ['nb', 'maxent', 'svm']
    multimodel_scores = {}
    for i in range(len(classifier_list)):
        multimodel_scores[classifier_list[i]] = [0 for _ in range(7)]

    df_train = prepareEntities(use_neutral=use_neutral, data_2=True)
    df_test = prepareEntities(use_neutral=use_neutral, data_2=False)

    res_multimodel = []

    skf = StratifiedKFold(n_splits=N_SPLIT, random_state=None, shuffle=False)
    for train_index, test_index in skf.split(df_test['content'], df_test['sentiment']):

        train = df_test.iloc[train_index]
        train = df_train.append(train)

        test = df_test.iloc[test_index]
        classification_SVM(train, test, use_neutral=use_neutral, polar=use_polar)
        res_multimodel.append(classification_multiple(train, test, use_polar=use_polar))

    for row in res_multimodel:
        for key, arr in row.items():
            multimodel_scores[key] = [x + y for x, y in
                                      zip(multimodel_scores[key], (np.array(arr) / len(classifier_list)))]

    #multiscore_report(multimodel_scores)
    exit()

