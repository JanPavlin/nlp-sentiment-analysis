# **Aspect-based sentiment analysis of SentiNews dataset**

###### Jaka Koren, Jan Pavlin.

How to run this file: 

##### **Text preprocessing**: To run text preprocessing you need to install the following python libraries:

- pandas
- nltk 
- caffe2
- stanza with Slovenian library 
- lemmagen 
- pickle
   
When you have all of those libraries installed you can run file "parserRun.py". 
The script will run for about 30 minutes and save the entities into file. 
The file is set to 'entities_24.5.pkl' but you can change it in 53th row of the script.


##### **Machine learning:** 
Machine learning algorithm is in file "main.py". 
To run this script without errors you need to install the following libraries:
- pandas
- pickle
- numpy 
- sklearn
- nltk 

If you run script "main.py" the learning of the Tf-idf-SVM model will start with k-fold parameter set to 3.
As result, for each training and predicting phase, scores for positive and negative sentiment will be 
printed out in console. 


**Because text preprocessing takes a long time, we recommend you to run only machine learning script "main.py".**