from parserRun import getStopWords
import pandas as pd
import pickle as pkl
import stanza


def stanzaPipe(sents):
    '''
    stanzaPipe from parser file.
    '''
    result = []
    pipe = stanza.Pipeline('sl', processors='tokenize,mwt,pos,lemma,depparse', tokenize_pretokenized=True)
    parsed = pipe(sents)
    print("Analysis complete.")

    for i, sentence in enumerate(parsed.sentences):
        res = set()
        for j, token in enumerate(sentence.tokens):
            res.add(token.words[0].lemma)

    return result


def edit_row(row):
    '''
    Edit text to lowercase and removes ".,", short words and stopwords.
    '''
    stopwords = getStopWords()
    words = row["content"].lower().replace(".", "").replace(",", "")
    words = words.split(" ")
    for i in range(len(words)-1, 0, -1):
        word = words[i]
        if len(word) < 3 or word in stopwords:
            del words[i]
            continue
    return words



def read_file():
    '''
    Opens file as pandas DataFrame and returns rows that are not neutral.
    '''
    filename = "SentiNews_document"
    fname = "SentiCoref_1.0/anotated/"+filename+"-level.txt"
    #paragraph:  38357
    #sentence: 72661
    #document: 5002
    tsvfile = pd.read_csv(fname, sep='\t')
    tsvfile = tsvfile[tsvfile.sentiment != "neutral"].reset_index(drop=True)

    return tsvfile

def save_pandas():
    '''
    loads the data from second dataset, prepares that data and proceeds with semantic analysis. Result is pandas
    DataFrame that is saved as pickle.
    '''

    filename = "second_dataset_12_5"
    processed = ""

    df = read_file()
    '''
    n = 0
    for index, row in df.iterrows():
        res = " ".join(edit_row(row))
        if n == 0:
            processed = res
        else:
            processed = processed + "." + " ".join(stanzaPipe(res))
        n += 1'''
    res = []

    for index, el in enumerate(processed.split(".")):
        res.append(stanzaPipe(el))

    df["processed"] = res
    print(df.info())
    pkl.dump(df, open("./"+filename+".pkl", "wb"))


if __name__ == '__main__':
    save_pandas()
