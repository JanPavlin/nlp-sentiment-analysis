import os
if __name__ == '__main__':
    '''
    goes over whole dataset and provide all negations
    (finds all occurrences of words ("ni", "ne") and saves them with the word following them. 
    '''
    senti_path = r'./SentiCoref_1.0'
    files = list()
    # list all files in dataset
    for file in os.listdir(senti_path):
        if file.endswith(".tsv"):
            files.append(file)
    tekst = ""
    # read all files and save their text
    for file in files:
        f = open(senti_path+"/"+file, "r", encoding="utf8")
        for i in range(6):
            f.readline()
        tekst += f.readline().split("=")[1]
    splt = tekst.split(" ")
    n = 0
    # go over all words in text and find occurences of words "ni" and "ne".
    # write them in a file
    f = open("negations", "w", encoding="utf8")
    for i in range(len(splt)):
        if splt[i] in ("ni", "ne"):
            print(splt[i], splt[i+1])
            n += 1
            f.write(splt[i]+" "+splt[i+1]+"\n")